package com.andriod.ecphone.Main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.andriod.ecphone.Model.SIPStackModel;
import com.andriod.ecphone.R;

import utils.DataTools;
import utils.ViewTools;

public class EcPhonePresenter implements EcPhoneContract.EcPresenter{
    private final MainActivity main;
    private final ViewTools vt;
    private SIPStackModel sipStackModel;
    boolean calling=false;
    boolean PttPressedOk = false;
    String[] notArray = null;
    private final String PTTPeer="666";

    public EcPhonePresenter(MainActivity main) {
        this.main = main;
        this.vt = new ViewTools(main);
    }

    @Override
    public void onAttach() {
        main.setupView();
        main.setupButtons();
        setupSipStack();
    }
        @Override
    public void onDetach() {
        main.DisplayLogs("on destroy");
        if (sipStackModel.SIPIsReady()) {
            main.DisplayLogs("Stop SipStack");
            sipStackModel.SIPStop();
        }
    }

    @Override
    public void onDial() {
        calling =true;
        main.DisplayLogs("Call on click");
        String number = main.mDestNumber.getText().toString().trim();
        if (number.length() < 1) {
            main.DisplayStatus("ERROR, Invalid destination number");
            return;
        }
        if (!sipStackModel.SIPIsReady()) {
            main.DisplayStatus("ERROR, cannot initiate call because SipStack is not started");
            return;
        }
        if (main.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            main.DisplayStatus("ERROR, No permission granted for microphone!");
            return;
        }
        sipStackModel.dialCall( number);
    }

    @Override
    public void onHangup() {
        main.DisplayLogs("Hangup on click");
        if (!sipStackModel.SIPIsReady())
            main.DisplayStatus("ERROR, cannot hangup because SipStack is not started");
        else
            sipStackModel.hangupCall();
    }

    @Override
    public void onAnswer() {
        main.DisplayLogs("Answer on click");
        if (!sipStackModel.SIPIsReady())
            main.DisplayStatus("ERROR, cannot Answer because SipStack is not started");
        else
            sipStackModel.answerCall(false);
    }

    @Override
    public void onPtt(MotionEvent event) {
        calling =true;
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            main.DisplayLogs("PTT Pressed");
            if (!PttPressedOk && sipStackModel.dialCall( PTTPeer)) {
                PttPressedOk = true;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            main.DisplayLogs("PTT Released");
            if (PttPressedOk) {
                PttPressedOk = false;
                sipStackModel.hangupCall();
            }
        }
    }

    @Override
    public void onRegUnReg() {
        main.DisplayLogs("ReG/Unreg clicked");
        if (!sipStackModel.SIPIsReady())
            main.DisplayStatus("ERROR, cannot Answer because SipStack is not started");
        else {
            sipStackModel.SIPRestart(vt.getTextString(R.id.sipAddress),vt.getTextString(R.id.SIPuser),vt.getTextString(R.id.SIPpass));
        }
    }

    @Override
    public void onKeyPressed(View v) {
        Button key=main.viewTools.getButton(v.getId());
        String newStr = main.mDestNumber.getText().toString()+key.getText().toString();
        main.mDestNumber.setText(newStr);
        main.mDestNumber.setSelection(main.mDestNumber.getText().length());
        main.viewTools.keyboardOff();
    }

    public void setupSipStack(){
        try{
            if (sipStackModel == null) {
                sipStackModel=new SIPStackModel(vt.getTextString(R.id.sipAddress),vt.getTextString(R.id.SIPuser),vt.getTextString(R.id.SIPpass),main);
                if (sipStackModel.SIPStart(NotifyThreadHandler))
                    main.DisplayLogs("Start SipStack");
                if (main.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(main, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
                }
                main.DisplayStatus("Ready.");
            } else {
                main.DisplayLogs("SipStack already started");
            }
        }catch (Exception e) { main.DisplayLogs("ERROR, StartSipStack"); }
    }

    public final Handler NotifyThreadHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            try {
                if (msg == null || msg.getData() == null) return;
                String receivedNotify = msg.getData().getString("notifmessages");
                if (receivedNotify != null && receivedNotify.length() > 0)
                    ReceiveNotifications(receivedNotify);
            } catch (Throwable e) {
                Log.e(MainActivity.logTag, "Notify Thread Handler handle Message");
            }
        }
    };
    public void ReceiveNotifications(String notifies) {
        if (notifies == null || notifies.length() < 1) return;
        notArray = notifies.split("\r\n");
        if (notArray.length < 1) return;
        for (int i = 0; i < notArray.length; i++) {
            if (notArray[i] != null && notArray[i].length() > 0) {
                if(notArray[i].indexOf("WPNOTIFICATION,") == 0) notArray[i] = notArray[i].substring(15); //remove the WPNOTIFICATION, prefix
                ProcessNotifications(notArray[i]);
            }
        }
    }
    public void ProcessNotifications(String notification) {
        main.DisplayStatus(notification);

        if (notification.indexOf("WPNOTIFICATION,") == 0)
        {
            notification = notification.substring(("WPNOTIFICATION,").length());
        }

        String[] params = notification.split(",");
        if(params.length < 2) return;
        params = DataTools.IncreaseArray(params,20);
        if(params[0].equals("STATUS") && (params[2].equals("Registered")||params[2].equals("Call Finished"))) {
            main.setBTNStatus(true, false, false, true);
            calling=false;
        }
        if(params[0].equals("STATUS") && params[2].equals("UnRegistered")) {
            main.setBTNStatus(false, false, false, false);
            calling=false;
        }
        if(params[0].equals("STATUS") && params[2].equals("Ringing"))
            main.setBTNStatus(false,!calling,true,PttPressedOk);

        if(params[0].equals("STATUS") && params[2].equals("Ringing")) {
            if(params[3].equals(PTTPeer) && !PttPressedOk) {
                main.setBTNStatus(false,false,true,false);
                main.DisplayStatus("Incoming call from " + params[3] + " " + params[6]);
                sipStackModel.answerCall(true);
                Toast.makeText(main, "PAGE", Toast.LENGTH_LONG).show();
            }else
                Toast.makeText(main, "Call From:"+params[3], Toast.LENGTH_LONG).show();
        }
    }
}
