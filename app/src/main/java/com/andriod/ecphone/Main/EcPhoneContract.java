package com.andriod.ecphone.Main;

import android.view.MotionEvent;
import android.view.View;

public interface EcPhoneContract {
    interface EcView{
        void DisplayLogs(String outStr);
        void setBTNStatus(boolean Dial,boolean Answer,boolean Hangup,boolean PTT);
        void DisplayStatus(String stat);
    }
    interface EcPresenter{
        void onAttach();
        void onDetach();
        void onDial();
        void onHangup();
        void onAnswer();
        void onPtt(MotionEvent event);
        void onRegUnReg();
        void onKeyPressed(View v);
        void setupSipStack();
    }
}
