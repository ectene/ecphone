package com.andriod.ecphone.Main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.andriod.ecphone.R;

import java.util.Calendar;
import java.util.TimeZone;

import utils.DataPersistence;
import utils.ViewTools;

public class MainActivity extends AppCompatActivity implements EcPhoneContract.EcView{
    public static final String logTag = "EcPhone";
    TextView mStatus = null;
    TextView mNotifications = null;
    Button mBtnDial = null;
    Button mBtnAnswer = null;
    Button mBtnHangup = null;
    Button mBtnPTT = null;
    Button mBtnRegUnReg = null;
    EditText mDestNumber = null;
    EcPhoneContract.EcPresenter presenter= new EcPhonePresenter(this);
    public ViewTools viewTools=new ViewTools(this);
    DataPersistence dataPersistence=new DataPersistence(this);
    int []keyIds={R.id.btn0,R.id.btn1,R.id.btn2,R.id.btn3,
            R.id.btn4,R.id.btn5,R.id.btn6,R.id.btn7,
            R.id.btn8,R.id.btn9,R.id.btnStar,R.id.btnSharp};
    public void setBTNStatus(boolean Dial,boolean Answer,boolean Hangup,boolean PTT){
        mBtnDial.setEnabled(Dial);
        mBtnAnswer.setEnabled(Answer);
        mBtnHangup.setEnabled(Hangup);
        mBtnPTT.setEnabled(PTT);
    }
    public void setupView(){
        mStatus = findViewById(R.id.status);
        mNotifications =  findViewById(R.id.notifications);
        mNotifications.setMovementMethod(new ScrollingMovementMethod());
        mDestNumber =  findViewById(R.id.SIPpath);
        mBtnDial =  findViewById(R.id.btnDial);
        mBtnAnswer =  findViewById(R.id.btnAnswer);
        mBtnHangup =  findViewById(R.id.btnHangup);
        mBtnPTT =  findViewById(R.id.btnPtt);
        mBtnRegUnReg = findViewById(R.id.btRegUn);
        dataPersistence.loadData("SIPAddr,SIPuser,SIPpass",R.id.sipAddress,R.id.SIPuser,R.id.SIPpass);
    }
    @SuppressLint("ClickableViewAccessibility")
    public void setupButtons(){
        setBTNStatus(false,false,false,false);
        View.OnClickListener KeyPadClickListener= v -> presenter.onKeyPressed(v);
        for(int id:keyIds)
            viewTools.getButton(id).setOnClickListener(KeyPadClickListener);
        mBtnDial.setOnClickListener(v -> presenter.onDial());
        mBtnHangup.setOnClickListener(v -> presenter.onHangup());
        mBtnAnswer.setOnClickListener(v -> presenter.onAnswer());
        mBtnPTT.setOnTouchListener((v, event) -> {
            presenter.onPtt(event);
            return true;
        });
        mBtnRegUnReg.setOnClickListener(v -> presenter.onRegUnReg());
        mDestNumber.requestFocus();
        viewTools.keyboardOff();
    }
    @Override
    protected void onPause() {
        super.onPause();
        dataPersistence.saveData("SIPAddr,SIPuser,SIPpass",R.id.sipAddress,R.id.SIPuser,R.id.SIPpass);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.onAttach();
    }

    public void DisplayStatus(String stat) {
        if (stat == null) return;
        if (mStatus != null) mStatus.setText(stat);
        DisplayLogs("Status: " + stat);
    }

    @SuppressLint("SimpleDateFormat")
    public void DisplayLogs(String logMsg) {
        if (logMsg == null || logMsg.length() < 1) return;

        if ( logMsg.length() > 2500) logMsg = logMsg.substring(0,300)+"...";
        logMsg = "["+ new java.text.SimpleDateFormat("HH:mm:ss:SSS").format(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime()) +  "] " + logMsg + "\r\n";

        Log.v(logTag, logMsg);
        if (mNotifications != null) mNotifications.append(logMsg);
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }
}