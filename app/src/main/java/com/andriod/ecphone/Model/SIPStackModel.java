package com.andriod.ecphone.Model;

import android.content.Context;
import android.os.Handler;

import com.mizuvoip.jvoip.SipStack;

public class SIPStackModel {
    String SIPServerAddress;
    String SIPUserName;
    String SIPPassword;
    SipStack mySipClient = null;
    Context ctx;
    GetNotificationsThread notifyThread = null;
    public SIPStackModel(String SIPServerAddress, String SIPUserName, String SIPPassword, Context ctx) {
        this.SIPServerAddress = SIPServerAddress;
        this.SIPUserName = SIPUserName;
        this.SIPPassword = SIPPassword;
        this.ctx=ctx;
    }
    public void SIPSetParameters(){
        String SIPFormat="serveraddress=%s\r\nusername=%s\r\npassword=%s\r\nloglevel=1";
        String SIPParams=String.format(SIPFormat,SIPServerAddress,SIPUserName,SIPPassword);
        mySipClient.SetParameters(SIPParams);
    }
    public boolean SIPStart(Handler NotifyThreadHandler) throws Exception{
        try{
            if (mySipClient == null) {
                mySipClient = new SipStack();
                mySipClient.Init(ctx);
                SIPSetParameters();
                notifyThread = new GetNotificationsThread();
                notifyThread.threadInit(mySipClient,NotifyThreadHandler);
                notifyThread.start();
                mySipClient.Start();
                return true;
            } else
                return false;
        }catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    public void SIPStop(){
        mySipClient.Stop(true);
        notifyThread.threadStop();
    }
    public boolean SIPIsReady(){
        return (mySipClient != null);
    }
    public void SIPRestart(String SIPServerAddress, String SIPUserName, String SIPPassword){
        this.SIPServerAddress = SIPServerAddress;
        this.SIPUserName = SIPUserName;
        this.SIPPassword = SIPPassword;
        mySipClient.Unregister();
        SIPSetParameters();
        mySipClient.Register();
    }
    public void hangupCall(){
        mySipClient.SetSpeakerMode(false);
        mySipClient.Hangup(-1);
    }
    public boolean dialCall(String number){
        return mySipClient.Call(-1,number);
    }
    public void answerCall(boolean speakerOn){
        if (speakerOn)
            mySipClient.SetSpeakerMode(speakerOn);
        mySipClient.Accept(-1);
    }
}
