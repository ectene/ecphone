package com.andriod.ecphone.Model;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.andriod.ecphone.Main.MainActivity;
import com.mizuvoip.jvoip.SipStack;

public class GetNotificationsThread extends Thread {
    SipStack mySipClient = null;
    private boolean terminateNotifyThread=false;
    private Handler NotifyThreadHandler=null;

    public void threadInit(SipStack mySipClient,Handler notifyThreadHandler) {
        this.mySipClient = mySipClient;
        NotifyThreadHandler = notifyThreadHandler;
    }
    public void threadStop(){
        terminateNotifyThread=true;
    }
    public void run()
    {
        try{
            try { Thread.currentThread().setPriority(4); } catch (Throwable e) { Log.e(MainActivity.logTag, "ERROR, Cant Set Thread Priority", e);}  //we are lowering this thread priority a bit to give more chance for our main GUI thread

            while (!terminateNotifyThread)
            {

                try{
                    String sipNotifications = "";
                    if (mySipClient != null)
                    {
                        //get notifications from the SIP stack
                        sipNotifications = mySipClient.GetNotificationsSync();

                        if (sipNotifications != null && sipNotifications.length() > 0)
                        {
                            // send notifications to Main thread using a Handler
                            Message messageToMainThread = new Message();
                            Bundle messageData = new Bundle();
                            messageToMainThread.what = 0;
                            messageData.putString("notifmessages", sipNotifications);
                            messageToMainThread.setData(messageData);

                            NotifyThreadHandler.sendMessage(messageToMainThread);
                        }
                    }

                    if ((sipNotifications == null || sipNotifications.length() < 1) && !terminateNotifyThread) {
                        //some error occurred. sleep a bit just to be sure to avoid busy loop
                        GetNotificationsThread.sleep(1);
                    }

                    continue;
                }catch(Throwable e){  Log.e(MainActivity.logTag, "ERROR, WorkerThread on run()intern", e); }
                if(!terminateNotifyThread) {
                    GetNotificationsThread.sleep(10);
                }
            }
        }catch(Throwable e){ Log.e(MainActivity.logTag, "WorkerThread on run()"); }
    }
}
