package utils;

public class DataTools {
    static public String[] IncreaseArray(String[] strArray,int len) {
        if(strArray.length >= len) return strArray;

        String[] newStrArray = new String[len];
        for(int i=0;i<strArray.length;i++)
        {
            if(strArray[i] != null)
                newStrArray[i] = strArray[i];
            else
                newStrArray[i] = "";
        }

        for(int i = strArray.length; i< 20; i++)
        {
            newStrArray[i] = "";
        }
        return newStrArray;
    }
}
