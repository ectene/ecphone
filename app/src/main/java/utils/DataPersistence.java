package utils;

import android.content.SharedPreferences;

import com.andriod.ecphone.Main.MainActivity;

public class DataPersistence {
    private SharedPreferences preferences;
    MainActivity main;
    public DataPersistence(MainActivity main) {
        this.main=main;
    }
    public void loadData(String fields,int...ids){
        SharedPreferences preferences = main.getPreferences(MainActivity.MODE_PRIVATE);
        if (preferences!=null) {
            int i=0;
            for(String field:fields.split(",")){
                main.viewTools.setTextString(ids[i], preferences.getString(field, null));
                i++;
            }
        }
    }
    public void saveData(String fields,int...ids){
        SharedPreferences preferences = main.getPreferences(MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        int i=0;
        for(String field:fields.split(",")){
            editor.putString(field,main.viewTools.getTextString(ids[i]));
            i++;
        }
        editor.apply();
    }
}
