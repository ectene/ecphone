package utils;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.andriod.ecphone.Main.MainActivity;

public class ViewTools {
    private MainActivity main;
    public ViewTools(MainActivity main) {
        this.main = main;
    }
    public String getTextString(int id){
        return ((EditText) main.findViewById(id)).getText().toString();
    }
    public void setTextString(int id,String instr){
        if (instr!=null && !instr.isEmpty())
            ((EditText) main.findViewById(id)).setText(instr);
    }
    public Button getButton(int id ){
        return main.findViewById(id);
    }
    public void keyboardOff(){
        main.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
